package com.example.arhw1;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Debug;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    public static boolean choice;
    public static int iid;
    public static int posiition;
    public static boolean rORf;
    public static Integer[] reals = {
            R.drawable.r1,
            R.drawable.r2,
            R.drawable.r3,
            R.drawable.r4,
            R.drawable.r5,
            R.drawable.r6,
            R.drawable.r7,
    };

    public static Integer[] fakes = {
            R.drawable.g1,
            R.drawable.g2,
            R.drawable.g3,
            R.drawable.g4,
            R.drawable.g5,
            R.drawable.g6,
            R.drawable.g7,
    };

    ImageView image_place = null;
    ImageView stamp_place = null;
    SeekBar confidence_bar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ImageButton startB = findViewById(R.id.start_b);


        new Thread(new Runnable() {
            public void run() {
                try {
                    InitConnection();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        startB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initGameButtons();
                switchImage();
                setImage();
                //System.out.println(iid);
            }
        });

    }

    int confidence_value = 50;
    void initGameButtons() {
        setContentView(R.layout.activity_game);
        confidence_bar = findViewById(R.id.confidence_setter);
        confidence_bar.setProgress(50);

        confidence_bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChangedValue = 0;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChangedValue = progress;
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                confidence_value = progressChangedValue;
                System.out.println("VALUE: "+confidence_value);
            }
        });


        ImageButton true_b = findViewById(R.id.true_b);
        final ImageButton false_b = findViewById(R.id.false_b);
        //ImageButton next_b = findViewById(R.id.next_b);

        image_place = findViewById(R.id.image_place);




        true_b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                choice = true;

                OnDecigionButtonClicked();
            }
        });

        false_b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                choice = false;
                OnDecigionButtonClicked();

            }
        });

//        next_b.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                switchImage();
//                setImage();
//                clearStamp();
//            }
//        });

        ImageButton help_b = findViewById(R.id.help_b);
        help_b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setContentView(R.layout.activity_guide);
                ImageButton back_b = findViewById(R.id.back_b);
                back_b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        setContentView(R.layout.activity_game);
                        initGameButtons();
                        switchImage();
                        setImage();
                    }


                });
            }
        });
    }

        void OnDecigionButtonClicked(){
            final String request_line = "{\"iid\": \""+iid+"\", \"choice\": \""+choice+"\", \"confidence\": \""+confidence_value+"\"}";
            //System.out.println(iid);
            new Thread(new Runnable() {
                public void run() {
                    try {
                        String answer =  SendAndReceive(request_line);
                        SetStatistics(answer);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
            setContentView(R.layout.activity_stats);
            ImageView image_place_stats = findViewById(R.id.image_place_stats);
            image_place_stats.setImageResource(iid);
            stamp_place = findViewById(R.id.stamp);
            checkImage();


            ImageButton back_b = findViewById(R.id.back_b);
            back_b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setContentView(R.layout.activity_game);
                    initGameButtons();
                    switchImage();
                    setImage();
                }
            });
            //System.out.println(iid);
        }
        static String stats;
        void SetStatistics(String answer){

            stats = "";
            String[] answer_splitted = answer.split(",");
            String percent_true = answer_splitted[0].split(":")[1];
            String percent_false = answer_splitted[1].split(":")[1];
            String confidence_true = answer_splitted[2].split(":")[1];
            String confidence_false = answer_splitted[3].split(":")[1];
//            System.out.println(percent_true);
//            System.out.println(percent_false);
//            System.out.println(confidence_true);
//            System.out.println(confidence_false);
            System.out.println(choice);
            if (choice){
                stats = "You think image is real!\n";
            }else{
                stats = "You think image is false!\n";
            }
            stats = stats + percent_true + " of people think this is real, with confidence: "+confidence_true+"\n";
            stats = stats + percent_false + " of people think this is fake, with confidence: "+confidence_false+"\n";

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    final TextView stats_place = findViewById(R.id.stats_place);
                    stats_place.setText(stats);
                }
            });



        }

    void checkImage() {
        if (rORf) {
            stamp_place.setImageResource(R.drawable.stampreal);
        } else {
            stamp_place.setImageResource(R.drawable.stampfake);
        }
    }

    void clearStamp() {
        stamp_place.setImageResource(R.drawable.clear);
    }

    void setImage() {

        if (rORf) {
            iid = reals[posiition];
            image_place.setImageResource(iid);

        } else {
            iid = fakes[posiition];
            image_place.setImageResource(iid);

        }
    }

    void switchImage() {
            //System.out.println("SWITCHED");
        Random r = new Random();
        rORf = r.nextBoolean();
        if (rORf) {
            posiition = r.nextInt(reals.length);
        } else {
            posiition = r.nextInt(fakes.length);
        }
    }

    String SendAndReceive(String message) throws IOException {
        PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
        out.println(message);
        BufferedReader in = new BufferedReader(new InputStreamReader(
                socket.getInputStream()));
        return in.readLine();
    }

    private static Socket socket;
     boolean InitConnection() throws IOException {
         boolean connection = true;
            //System.out.println("Start sending");
         try
         {
             String host = "35.210.180.227";
             int port = 9999;
             InetAddress address = InetAddress.getByName(host);
             socket = new Socket(address, port);

         }
         catch (Exception exception)
         {
             exception.printStackTrace();
             connection = false;
         }
         return connection;
    }

    boolean CloseSocket (){
         boolean connection = true;

        try
        {
            socket.close();
        }

        catch(Exception e)
        {
            e.printStackTrace();
            connection = false;
        }
        return connection;
    }
}
